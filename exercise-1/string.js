function ucfirst(str) {
    if (typeof str !== "string" || str.length === 0) {
        return "";
    }

    return str[0].toUpperCase() + str.substr(1);
}

function capitalize(str) {
    if (typeof str !== "string" || str.length === 0) {
        return "";
    }

    return str.toLowerCase().split(" ").map(item => ucfirst(item)).join(" ");
}

function camelCase(str) {
    if (typeof str !== "string" || str.length === 0) {
        return "";
    }

    return str.replace(/[^a-zA-Z0-9 ]/g, "").toLowerCase().split(" ").map(item => ucfirst(item)).join("");
}

function snakeCase(str) {
    if (typeof str !== "string" || str.length === 0) {
        return "";
    }

    return str.replace(/[^a-zA-Z0-9 ]/g, "").toLowerCase().split(" ").map(item => ucfirst(item)).join("_");
}

function leet(str) {
    if (typeof str !== "string" || str.length === 0) {
        return "";
    }

    let code = {
        "a": "4",
        "e": "3",
        "i": "1",
        "o": "0",
        "u": "(_)",
        "y": "7",
        "A": "4",
        "E": "3",
        "I": "1",
        "O": "0",
        "U": "(_)",
        "Y": "7",
    }

    return str.replace(/[aeiouyAEIOUY]/g, code);
}
