function type_check_v1(variable, type) {
    if(Array.isArray(variable)) return type === "array";
    if(variable === null) return type === "null";
    return typeof variable === type;
}

function type_check_v2(arg, type) {
    let result = true;
    if ("type" in type) if(!type_check_v1(arg, type["type"])) result = false; 
    if ("value" in type) if(arg !== type["value"]) result = false;
    if ("enum" in type) {
        if (typeof arg === "object") {
            let jsonArg = JSON.stringify(arg);
            let result_2 = false;
            type['enum'].forEach(element => {
                console.log(JSON.stringify(element));
                console.log(jsonArg);
                if (JSON.stringify(element) == jsonArg) result_2 = true;
            });
            result = result_2;
        } else {
            if(!type['enum'].includes(arg)) result = false;
        }
    } 
    return result;
}

console.log(type_check_v2({prop1:1}, {type:"object"}));
console.log(type_check_v2("foo", {type:"string", value:"foo"}));
console.log(type_check_v2("bar", {type:"string", value:"foo"}));
console.log(type_check_v2({id:1, name:"fedf"}, {enum:["foo", "bar", {id:1, name:"fedf"}]}));